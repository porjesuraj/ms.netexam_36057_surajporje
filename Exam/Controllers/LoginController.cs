﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Exam.Models;

namespace Exam.Controllers
{
    public class LoginController : Controller
    {
        private SunbeamDBEntities sdObj = new SunbeamDBEntities();

        public ActionResult Signin()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SignIn(EmployeeDetail loginObject, string ReturnUrl)
        {

            var MatchCount = (from emp in sdObj.EmployeeDetails.ToList()
                              where emp.Email_id.ToLower() == loginObject.Email_id.ToLower()
                             && emp.password.ToLower() == loginObject.password.ToLower()
                              select emp).ToList().Count();

            if (MatchCount != 0)
            {
                

                FormsAuthentication.SetAuthCookie(loginObject.Email_id, true);



              

                if (ReturnUrl != null)
                {
                    return Redirect(ReturnUrl);

                }
                else
                {

                    return Redirect("/Home/Index");
                }


            }
            else
            {
                ViewBag.ErrorMessage = "User Name or Password is incorrect";
                return View();
            }


        }

        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();

            return Redirect("/Home/Index");
        }



    }
}
